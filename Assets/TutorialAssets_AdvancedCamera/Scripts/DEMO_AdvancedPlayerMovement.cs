﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEMO_AdvancedPlayerMovement : MonoBehaviour
{
    //Public floats
    public float movementSpeed;
    public float rotateSpeed;
    public float jumpForce;
    public float jumpDamp;
    public float fallMultiplier;

    //Private floats
    private Rigidbody _rb;
    private bool _shouldJump = false;
    private bool _isGrounded = false;
    private bool _canDouble = true;

    // Start is called before the first frame update
    void Start()
    {
        //Cache our rigidbody reference
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //Handle our jump input in the Updated function due to how GetKeyDown is called and due to Unity possibly missing a jump call if it
        //is called from FixedUpdate
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //If we are on the ground and we can jump, we can then jump
            if (!_shouldJump && _isGrounded)
            {
                //Set our bool to true so we calculate a jump in our fixed update
                _shouldJump = Input.GetKeyDown(KeyCode.Space);
            }
        }

        //Here's where we control out how player jumps
        //We add a damp to make us jump faster at the beginning, but slow down as we reach our peak velocity
        if(_rb.velocity.y > 0)
        {
            _rb.velocity += Vector3.up * Physics.gravity.y * (1 - jumpDamp) * Time.deltaTime;
        }
        //We add a fall multiplier to fall faster the longer we fall
        if(_rb.velocity.y < 0)
        {
            _rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
    }

    //Check to see the our character has colided with the ground and can jump again
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            //Set our jumping bools true so we can jump once we're on the ground
            _isGrounded = true;
            _canDouble = true;
        }
    }

    //We call our physics in FixedUpdate so we get accurate calculations independent of the frame rate
    private void FixedUpdate()
    {
        //Get our input values
        float verticalMovement = Input.GetAxis("Vertical") * movementSpeed;

        //if our input is not 0, move our character
        if(verticalMovement != 0)
        //Set our player's velocity on the forward and up axis
        _rb.velocity = (transform.forward * verticalMovement) + new Vector3(0,_rb.velocity.y,0);

        //Get our input values
        float verticalRotation = Input.GetAxis("Horizontal");
        //Get our player's rotation
        Vector3 playerRot = transform.localRotation.eulerAngles;
        //Set our player's rotation
        transform.localEulerAngles = new Vector3(0, playerRot.y + verticalRotation * rotateSpeed, 0);
        //This is our jump call
        //If _shouldJump is true, we can add force to our character
        if (_shouldJump)
        {
            _rb.AddForce(Vector3.up * jumpForce);
            //If we have double jumped, don't let us jump until we've grounded
            if (!_canDouble)
            {
                _isGrounded = false;
            }
            //Prevent us from jumping more than our fairshare
            _shouldJump = false;
            _canDouble = false;
        }

    }
}
