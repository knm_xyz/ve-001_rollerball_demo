﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debug_PlayerVectors : MonoBehaviour
{
    public Transform player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.position.x, transform.position.y, player.position.z);
        transform.localEulerAngles = new Vector3(0, player.localRotation.eulerAngles.y, 0);
    }
}
