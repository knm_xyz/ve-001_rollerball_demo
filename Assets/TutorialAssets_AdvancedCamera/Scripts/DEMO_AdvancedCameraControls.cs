﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEMO_AdvancedCameraControls : MonoBehaviour
{
    //Public Variables
    public enum ControlState { Locked = 1, Free, Controlled }
    public ControlState cameraControlState;
    [Space (10)]
    public Transform player;
    public float cameraRotationSpeed;
    public float cameraZoomSensitivity;
    public Vector3 cameraOffsetPosition;
    public Vector3 firstPersonPosition;

    //Private variables
    private float cameraLerpPct = 0;
    private Transform _cameraRigParent;
    //Serializing a private variable allows you to edit it in the inspector
    [SerializeField] private Transform _camera;
    [SerializeField] private Transform _cameraControlParent;

    // Start is called before the first frame update
    void Start()
    {
        //This isn't necessary since it's this.transform, but it comes in handy later when we're rotating different things
        _cameraRigParent = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        //Our rig parent will always follow our player
        _cameraRigParent.position = new Vector3(player.position.x, 0, player.position.z);

        //Get our input values
        float camRotation = Input.GetAxis("CameraHorizontal");
        float scrollWheelInput = Input.GetAxis("Mouse ScrollWheel");

        //always allow our camera to lerp between a zoomed in/out position
        //If our input is > 0
        if (scrollWheelInput != 0)
        {
            //Keep track of how far we've scrolled
            cameraLerpPct += scrollWheelInput;

            //If our scroll is farther than our max, set it back so we don't get stuck
            //We're using this value to define our range for when we remap
            if (cameraLerpPct >= cameraZoomSensitivity)
                cameraLerpPct = cameraZoomSensitivity - 0.01f;
            //If it's less, set it forward to not get stuck
            if (cameraLerpPct <= 0)
                cameraLerpPct = 0.001f;
        }
        //Remap our CameraLerpPct so we get a nice number between 0 - 1  
        float mappedPct = Remap(cameraLerpPct, 0, cameraZoomSensitivity, 0, 1);
        //Lerp our camera position based on our remapped percent
        _camera.localPosition = Vector3.Lerp(cameraOffsetPosition, firstPersonPosition, mappedPct);

        //This switch case changes our camera functionality based on our control state
        switch (cameraControlState)
        {
            //Camera always behind our character by following their rotation
            case ControlState.Locked:
                _cameraRigParent.localEulerAngles = new Vector3(0, player.localRotation.eulerAngles.y, 0);
                _cameraControlParent.localEulerAngles = Vector3.zero;
                break;
            //Camera follows our player's rotation, but can also rotate freely
            case ControlState.Controlled:
                _cameraRigParent.localEulerAngles = new Vector3(0, player.localRotation.eulerAngles.y, 0);
                _cameraControlParent.localEulerAngles += Vector3.up * cameraRotationSpeed * camRotation;
                break;
            //Our camera will move freely of our player's rotation
            case ControlState.Free:
                _cameraRigParent.localEulerAngles += Vector3.up * cameraRotationSpeed * camRotation;
                _cameraControlParent.localEulerAngles = Vector3.zero;
                break;
        }
    }

    //Remap isn't built into Unity, so here's the equation. This is used to scale a value to a new range based on it's position within an old range
    public static float Remap(float value, float minOld, float maxOld, float minNew, float maxNew)
    {
        float mappedVal = minNew + (maxNew - minNew) * ((value - minOld) / (maxNew - minNew));
        return mappedVal;
    }
}
    