﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class DEMO_GameController : MonoBehaviour
{
    public bool debug;
    //Public variables
    public GameObject startScreen;
    public GameObject endScreen;
    public GameObject countdownScreen;
    public GameObject UIScreen;
    public TextMeshProUGUI countdownText;
    public string firstScene;
    public string secondScene;
    public int countdownStart;

    public UnityEvent resetGame;
    public UnityEvent OnGameStart;

    public DEMO_PlayerController playerController;
    public DEMO_PickupSpawner pickupSpawner;

    // Start is called before the first frame update
    void Start()
    {
        if (debug)
        {
            OnGameStart.Invoke();
        }
        else
        {
            //Turn the start screen on at the start
            startScreen.SetActive(true);
            //Turn the end screen off
            endScreen.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //this is called to start the countdown
    public void StartGame()
    {
        //Set the start screen on
        startScreen.SetActive(false);
        //Start the countdown
        StartCoroutine(Countdown(countdownStart));
    }

    //This method will load the chosen scene
    public void LoadNextScene(string _sceneName)
    {
        //Load scene in the scene manager by name
        SceneManager.LoadScene(_sceneName, LoadSceneMode.Single);
    }

    //Resets the game
    public void ResetGame()
    {
        //Calls the Unity Event "Reset Game"
        resetGame.Invoke();
    }

    //Coroutine for the countdown
    IEnumerator Countdown(int _start)
    {
        //Set the text to start at our starting number
        countdownText.text = _start.ToString();
        //Turn the countdown screen on
        countdownScreen.SetActive(true);
        //Wait for 0.1s
        yield return new WaitForSeconds(0.1f);
        //run through a for loop to count down
        for(int i = _start; i >= 0; i--)
        {
            //Set the text to the current number
            countdownText.text = i.ToString();
            //If it's less than one, change it to start
            if(i < 1)
            {
                countdownText.text = "START!";
            }
            //Wait 1 second between each for loop round
            yield return new WaitForSeconds(1.0f);
            //Return to the start of the for loop
            yield return null;
        }

        OnGameStart.Invoke();
        //End the coroutine
        yield break;
    }
}
