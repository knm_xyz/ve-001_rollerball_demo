﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController_v02 : MonoBehaviour
{
    public float movementSpeed = 10;
    public float rotationSpeed = 5;
    public float jumpForce = 300f;
    public float jumpDamp;
    public float fallMultiplier = 2.5f;

    private Rigidbody _rb;
    private bool _shouldJump = false;
    private bool _isGrounded = false;
    private bool _canDouble = true;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!_shouldJump && _isGrounded)
            {
                _shouldJump = Input.GetKeyDown(KeyCode.Space);
            }

        }

        if(_rb.velocity.y > 0)
        {
            _rb.velocity += Vector3.up * Physics.gravity.y * (1 - jumpDamp) * Time.deltaTime;
        }

        if(_rb.velocity.y < 0)
        {
            _rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.CompareTag("Ground"))
        {
            _isGrounded = true;
            _canDouble = true;
        }
    }

    private void FixedUpdate()
    {
        float verticalMove = Input.GetAxis("Vertical");
        float horizontalMove = Input.GetAxis("Horizontal");

        if (verticalMove != 0)
            _rb.velocity = (transform.forward * verticalMove * movementSpeed) + new Vector3(0, _rb.velocity.y, 0); ;

        Vector3 playerRot = transform.localRotation.eulerAngles;

        transform.localEulerAngles = new Vector3(0, playerRot.y + horizontalMove * rotationSpeed, 0);

        if (_shouldJump)
        {
            if (!_canDouble)
            {
                _isGrounded = false;
            }

            _rb.AddForce(Vector3.up * jumpForce);
            _shouldJump = false;
            _canDouble = false;
        }
    }
}
