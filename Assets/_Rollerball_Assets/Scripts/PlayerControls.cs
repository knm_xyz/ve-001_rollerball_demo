﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerControls : MonoBehaviour
{
    public float movementSpeed = 10;
    public GameObject winScreen;
    public TextMeshProUGUI countText;

    private Rigidbody _rb;
    private int count = 0;

    // Start is called before the first frame update
    void Start()
    {
        _rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(count >= 2)
        {
            Debug.Log("You Win!");
            winScreen.SetActive(true);
        }
    }


    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);

        _rb.AddForce(movement * movementSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            count += 1;
            countText.text = "Count: " + count.ToString();
            Destroy(other.gameObject);
        }
    }
}
