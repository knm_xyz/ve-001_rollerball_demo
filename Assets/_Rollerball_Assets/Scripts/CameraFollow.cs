﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public enum ControlState { Locked = 1, Controlled, Free}
    public ControlState cameraControlState;

    public Transform player;
    public Vector3 cameraOffsetPosition;
    public Vector3 firstPersonPosition;
    public float cameraRotationSpeed = 5;
    public float cameraZoomSensitivity = 3;

    private Transform _cameraParentRig;
    [SerializeField] private Transform _cameraControlParent;
    [SerializeField] private Transform _camera;
    private float cameraLerpPct = 0;

    // Start is called before the first frame update
    void Start()
    {
        _cameraParentRig = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        //Happen always
        _cameraParentRig.position = new Vector3(player.position.x, 0, player.position.z);

        float camRotation = Input.GetAxis("CameraHorizontal");
        float scrollWheelInput = Input.GetAxis("Mouse ScrollWheel");

        if (scrollWheelInput != 0)
        {
            cameraLerpPct += scrollWheelInput;

            if (cameraLerpPct >= cameraZoomSensitivity)
                cameraLerpPct = cameraZoomSensitivity - 0.01f;
            if (cameraLerpPct <= 0)
                cameraLerpPct = 0.001f;
        }

        float mappedPct = Remap(cameraLerpPct, 0, cameraZoomSensitivity, 0, 1);
        _camera.localPosition = Vector3.Lerp(cameraOffsetPosition, firstPersonPosition, mappedPct);

        switch (cameraControlState)
        {
            case ControlState.Locked:
                _cameraParentRig.localEulerAngles = new Vector3(0, player.localRotation.eulerAngles.y, 0);
                _cameraControlParent.localEulerAngles = Vector3.zero;
                break;
            case ControlState.Controlled:
                _cameraParentRig.localEulerAngles = new Vector3(0, player.localRotation.eulerAngles.y, 0);
                _cameraControlParent.localEulerAngles += Vector3.up * cameraRotationSpeed * camRotation;
                break;
            case ControlState.Free:
                _cameraParentRig.localEulerAngles += Vector3.up * cameraRotationSpeed * camRotation;
                _cameraControlParent.localEulerAngles = Vector3.zero;
                break;
        }
    }

    public static float Remap(float value, float minOld, float maxOld, float minNew, float maxNew)
    {
        float mappedVal = minNew + (maxNew - minNew) * ((value - minOld) / (maxNew - minNew));
        return mappedVal;
    }
}
