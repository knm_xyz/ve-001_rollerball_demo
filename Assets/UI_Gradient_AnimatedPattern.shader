// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "UI_Gradient_AnimatedPattern"
{
	Properties
	{
		_Gradient1Color2("Gradient 1 - Color 2", Color) = (0,0,0,1)
		_Gradient1Color1("Gradient 1 - Color 1", Color) = (1,1,1,1)
		_Gradient2Color2("Gradient 2 - Color 2", Color) = (0,0.0543108,1,1)
		_Gradient2Color1("Gradient 2 - Color 1", Color) = (1,0,0,1)
		_BackgroundTexture("Background Texture", 2D) = "white" {}
		_RotationSpeed("Rotation Speed", Float) = 0
		_TextureST("Texture ST", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Custom"  "Queue" = "Transparent+0" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha , SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Gradient1Color1;
		uniform float4 _Gradient1Color2;
		uniform float4 _Gradient2Color1;
		uniform float4 _Gradient2Color2;
		uniform float _RotationSpeed;
		uniform sampler2D _BackgroundTexture;
		uniform float4 _TextureST;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 lerpResult3 = lerp( _Gradient1Color1 , _Gradient1Color2 , i.uv_texcoord.y);
			float mulTime17 = _Time.y * _RotationSpeed;
			float cos15 = cos( mulTime17 );
			float sin15 = sin( mulTime17 );
			float2 rotator15 = mul( i.uv_texcoord - float2( 0.5,0.5 ) , float2x2( cos15 , -sin15 , sin15 , cos15 )) + float2( 0.5,0.5 );
			float4 lerpResult9 = lerp( _Gradient2Color1 , _Gradient2Color2 , rotator15.y);
			float2 appendResult26 = (float2(_TextureST.x , _TextureST.y));
			float2 appendResult27 = (float2(_TextureST.z , _TextureST.w));
			float2 uv_TexCoord20 = i.uv_texcoord * appendResult26 + ( appendResult27 * _Time.y );
			float4 lerpResult6 = lerp( lerpResult3 , lerpResult9 , tex2D( _BackgroundTexture, uv_TexCoord20 ));
			o.Emission = lerpResult6.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18400
489;643;932;436;3130.347;432.4645;3.596188;True;False
Node;AmplifyShaderEditor.Vector4Node;25;-1703.487,738.7173;Inherit;False;Property;_TextureST;Texture ST;8;0;Create;True;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;18;-1886.883,642.5055;Inherit;False;Property;_RotationSpeed;Rotation Speed;7;0;Create;True;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;23;-1559.671,948.7495;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;16;-1677.657,496.1573;Inherit;False;Constant;_Vector0;Vector 0;8;0;Create;True;0;0;False;0;False;0.5,0.5;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;12;-1725.283,365.6666;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;17;-1693.883,646.5055;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;27;-1486.294,848.9661;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;26;-1438.36,700.6707;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-1353.198,888.2474;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RotatorNode;15;-1449.669,384.6701;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;20;-1228.817,728.977;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;1;-1151.104,-499.6681;Inherit;False;Property;_Gradient1Color1;Gradient 1 - Color 1;3;0;Create;True;0;0;False;0;False;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;7;-1153.123,21.81731;Inherit;False;Property;_Gradient2Color1;Gradient 2 - Color 1;5;0;Create;True;0;0;False;0;False;1,0,0,1;1,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;8;-1150.119,194.9732;Inherit;False;Property;_Gradient2Color2;Gradient 2 - Color 2;4;0;Create;True;0;0;False;0;False;0,0.0543108,1,1;0,0.0543108,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;19;-1205.864,478.716;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.ColorNode;4;-1153.711,-322.9122;Inherit;False;Property;_Gradient1Color2;Gradient 1 - Color 2;2;0;Create;True;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;14;-1156.504,-126.71;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;3;-884.9539,-339.7673;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;9;-854.1685,178.3566;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;11;-883.4666,643.7456;Inherit;True;Property;_BackgroundTexture;Background Texture;6;0;Create;True;0;0;False;0;False;-1;None;None;True;1;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;6;-564.2277,169.3553;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-1572.389,-275.706;Inherit;False;Property;_Gradient1Blend;Gradient 1 Blend;1;0;Create;True;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;2;-230.7788,-323.5506;Inherit;False;Gradient1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;93,-9;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;UI_Gradient_AnimatedPattern;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;False;Custom;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;2;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;True;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;17;0;18;0
WireConnection;27;0;25;3
WireConnection;27;1;25;4
WireConnection;26;0;25;1
WireConnection;26;1;25;2
WireConnection;24;0;27;0
WireConnection;24;1;23;0
WireConnection;15;0;12;0
WireConnection;15;1;16;0
WireConnection;15;2;17;0
WireConnection;20;0;26;0
WireConnection;20;1;24;0
WireConnection;19;0;15;0
WireConnection;3;0;1;0
WireConnection;3;1;4;0
WireConnection;3;2;14;2
WireConnection;9;0;7;0
WireConnection;9;1;8;0
WireConnection;9;2;19;1
WireConnection;11;1;20;0
WireConnection;6;0;3;0
WireConnection;6;1;9;0
WireConnection;6;2;11;0
WireConnection;0;2;6;0
ASEEND*/
//CHKSM=60A489FB719D31CE876C9AF24D47BCB353C2B4A5